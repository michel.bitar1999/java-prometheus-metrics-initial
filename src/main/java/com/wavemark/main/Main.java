package com.wavemark.main;
import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;
import io.prometheus.client.Histogram;
import io.prometheus.client.Summary;
import io.prometheus.client.exporter.HTTPServer;
import com.wavemark.Server.JavaHTTPServer;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Date;
import java.util.Random;

public class Main {

    static final int PORT = 8080;
    static final boolean verbose = true;


    private static double rand(double min, double max) {
        return min + (Math.random() * (max - min));
    }

    private static int[] getInterfaces(){
        int[] interfaces = {0,0,0};
        for(int i = 0;i<3;i++){
            int num =(int) rand(1,1000) % 2;
            interfaces[i] = num;
        }
        return interfaces;
    }

    public static void main(String[] args) {
        Counter counter = Counter.build().namespace("java").name("my_counter").help("This is my counter").register();
        Histogram histogram = Histogram.build().buckets(200,400,600,800).namespace("java").name("my_histogram").help("This is my histogram").register();

       /* Gauge firstInterfaceGauge = Gauge.build().name("Interface_1").help("Gauge for Interface 1").register();
        Gauge secondInterfaceGauge = Gauge.build().name("Interface_2").help("Gauge for Interface 2").register();
        Gauge thirdInterfaceGauge = Gauge.build().name("Interface_3").help("Gauge for Interface 3").register();*/

        Gauge gauge = Gauge.build().name("Interfaces").labelNames("Interface_id").help("This is my gauge").register();
        //labelNames("Interface_1","Interface_2","Interface_3")


        // Summary summary = Summary.build().namespace("java").name("my_summary").help("This is my summary").register();
       Summary requestSleep = Summary.build()
                .quantile(0.90, 0.05)   // Add 50th percentile (= median) with 5% tolerated error
                .quantile(0.95, 0.01)
                .quantile(0.99,0)
                .name("requests_sleep").help("Request latency in seconds.").register();

        int randRequestDuration = (int) Math.pow(Math.random() + 1.0, 14);


        Thread bgThread = new Thread(() -> {
            while (true) {
                try {
                   // counter.inc(1);
                   // gauge.set(rand(-5, 10));
                    //histogram.observe(rand(0, 5));
                   // summary.observe(rand(0, 5));

                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });


        Thread apiThread = new Thread(() -> {


            try {
                ServerSocket serverConnect = new ServerSocket(PORT);
                System.out.println("Server started.\nListening for connections on port : " + PORT + " ...\n");
                // we listen until user halts server execution
                while (true) {
                    JavaHTTPServer myServer = new JavaHTTPServer(serverConnect.accept());
                    double sleepTime = rand(1,1000);
                    double sleepTime2 = (int) (Math.pow(Math.random() + 1.0, 14) / 16);

                    try {
                        Thread.sleep((long) sleepTime);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    if (verbose) {
                        System.out.println("Connecton opened. (" + new Date() + ")");
                    }

                    int[] interfaces = getInterfaces();

                    for(int i = 0;i<3;i++){
                        String Label = "Interface_" + (i+1);
                        //System.out.println("LABEL: " + Label);
                        if(interfaces[i] == 0)
                           gauge.labels(Label).set(0);
                        else
                            gauge.labels(Label).set(1);
                    }
/*
                    if(interfaces[0] == 0)
                        firstInterfaceGauge.set(0);
                    else
                        firstInterfaceGauge.set(1);

                    if(interfaces[1] == 0)
                        secondInterfaceGauge.set(0);
                    else
                        secondInterfaceGauge.set(1);

                    if(interfaces[2] == 0)
                        thirdInterfaceGauge.set(0);
                    else
                        thirdInterfaceGauge.set(1);*/

                    counter.inc(1);
                    histogram.observe(sleepTime);
                    requestSleep.observe(sleepTime2);
                    System.out.println("Date: " + new Date() + " -- Count : " + counter.get());
                    System.out.println("Date: " + new Date() + " -- Histogram Increase : " + sleepTime);
                    // create dedicated thread to manage the client connection
                    Thread thread = new Thread(myServer);
                    thread.start();
                }

            } catch (IOException e) {
                System.err.println("Server Connection error : " + e.getMessage());
            }


        });

        bgThread.start();
        apiThread.start();


        try {

            HTTPServer server = new HTTPServer(12345);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}